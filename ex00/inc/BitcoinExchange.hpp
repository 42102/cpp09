
#ifndef BITCOIN_EXCHANGE_HPP
#define BITCOIN_EXCHANGE_HPP

#include <iostream>
#include <map>
#include <stdlib.h>
#include <string>
#include <fstream>

#define RED "\033[31m"
#define BLUE "\033[34m"
#define GREEN "\033[32m"
#define RESET "\033[0m"

class BitcoinExchange
{
	private:
		std::string _name_database;
		std::map<std::string,float> _exchange_values;
		std::multimap<std::string, float> _data_to_compare;

	void check_database_io_state(std::ifstream& in);
		
	public:
		BitcoinExchange();
		void add_field_to_database(const std::string& data);
		void add_entry_to_compare(const std::string& data_comp);
		void read_data_to_compare(char * input_file);
		void show_databases_values(void);
		void show_values_to_compare(void);
		void converting_values(void);
};

class Date
{
	private:
		size_t _day;
		size_t _month;
		size_t _year;
		bool parse_date(const std::string& date);

	public:
		
		class BadDateException: public std::exception
		{
			private:
				std::string _msg;

			public:
				BadDateException(void);
				~BadDateException() throw();
				virtual const char * what() const throw();
		};

		Date(const std::string& date);
		Date(void);
		bool operator<= (const Date& date) const;
		bool operator> (const Date& date) const;
		bool operator< (const Date& date) const;
		Date& operator= (const Date& date_cpy);

};

bool parse_date(std::string date);
bool parse_float(std::string value);

#endif
