
#include "../inc/BitcoinExchange.hpp"

/*Constructors and destructor*/
BitcoinExchange::BitcoinExchange()
{
	std::string data;

	this->_name_database = "./data.csv";
	std::ifstream input;

	input.open(this->_name_database.c_str());
	if(input.fail() == true)
		std::cout<<"error: cannot open file: "
			<<this->_name_database<<std::endl;

	std::getline(input, data, '\n');
	std::getline(input, data, '\n');
	check_database_io_state(input);
	while(input.eof() == false)
	{
		if(data.empty() == false)
		{
			add_field_to_database(data);
		}
		std::getline(input, data, '\n');
		check_database_io_state(input);
	}
	input.close();
}

/*Methods of BitcoinExchange class*/
void BitcoinExchange::add_field_to_database(const std::string& data)
{
	int init;
	int pos;
	int separator;
	int pos_separator;
	bool valid_date;
	bool valid_float;
	float value_number;
	std::string date;
	std::string value;
	
	init = 0;
	pos = init;
	separator = 0;
	valid_date = false;

	while(data[pos] != '\0')
	{
		if(data[pos] == ',' && separator == 0)
		{
			pos_separator = pos + 1;
			separator++;
			date = data.substr(init, pos - init);
			valid_date = parse_date(date);
		}
		pos++;
	}
	if(separator > 1 || valid_date == false)
		std::cout<<"error inserting : "<<data<<std::endl;
	else
	{
		value = data.substr(pos_separator);
		valid_float = parse_float(value);
		if(valid_float == false)
			std::cout<<"error inserting: "<<data<<std::endl;
		else
		{
			try
			{
				value_number =  stod(value);
			}
			catch(std::exception &e)
			{
				std::cout<<"error: "<<e.what()<<std::endl;
			}
			this->_exchange_values[date] = value_number;
		}
	}
}


/*
 * Method to parse dates, returns true if date passed is correc,
 * false in another situation
 * */
bool parse_date(std::string date)
{
	int pos;
	int init;
	int field_of_date;
	std::string field;
	int year;
	int month;
	int day;
	int separator;
	std::map<int, int> month_days;

	month_days[1] = 31;
	month_days[3] = 31;
	month_days[4] = 30;
	month_days[5] = 31;
	month_days[6] = 30;
	month_days[7] = 31;
	month_days[8] = 31;
	month_days[9] = 30;
	month_days[10] = 31;
	month_days[11] = 30;
	month_days[12] = 31;	

	init = 0;
	pos = 0;
	field_of_date = 0;
	separator = 0;
	while(date[pos] != '\0')
	{
		if(!(date[pos] >= '0' && date[pos] <= '9') && date[pos] != '-')
			return false;
		if(date[pos] == '-')
		{
			separator++;
			if(field_of_date == 0)
			{
				field = date.substr(init, pos - init);
				if(field.size() > 10 && field.size() < 1)
					return false;
				year = atoi(field.c_str());
			}
			else if(field_of_date == 1)
			{
				field = date.substr(init, pos - init);
				if(field.size() != 2 )
					return false;
					month = atoi(field.c_str());
					if(month <= 0 || month > 12)
						return false;
			}
			else if(separator > 2)
				return false;

			field_of_date++;
			init = pos + 1;
		}
		pos++;
	}
	field = date.substr(init, pos - init);
	
	if(field.size() != 2)
		return false;
	day = atoi(field.c_str());
	if(month != 2)
	{
		if(day > month_days[month] || day < 0)
			return false;
	}
	else
	{
		if((year % 4) == 0)
		{
			if(day > 29 || day < 0)
				return false;
		}
		else if(day > 28 || day < 0)
			return false;

	}

	return true;
}

/*
 * Method to parse float numbers, returns true if number is correct,
 * false in other situation*/
bool parse_float(std::string value)
{
	int points_found;
	size_t pos;
	bool not_zero;
	double number;

	points_found = 0;	
	pos = 0;
	if(value.size() > 39)
		return false;
	while(value[pos] != '\0')
	{
		if(!(value[pos] >= '0' && value[pos] <= '9') && value[pos] != '.')
			return false;
		if(value[pos] != '0' && value[pos]!= '.')
			not_zero = true;
		if(value[pos] == '.')
			points_found++;
		pos++;
	}

	if(points_found > 1)
		return false;
	try
	{	
		number = stod(value);
	}
	catch(std::exception &e)
	{
		std::cout<<"error: "<<e.what()<<std::endl;
		return false;
	}

	if(number > std::numeric_limits<float>::max() || number < -(std::numeric_limits<float>::max()))
	{
		return false;
	}
	return true;
}

void BitcoinExchange::check_database_io_state(std::ifstream& in)
{
	if(in.eof() == false && in.fail() == true)
		{
			std::cout<<"error: bad operation reading file"<<std::endl;
			std::cout<<"run again pls!!!"<<std::endl;
			in.close();
			exit(-1);
		}

}

/*
 * Method to add entries to database
 * */
void BitcoinExchange::add_entry_to_compare(const std::string& data_comp)
{
	bool valid_date;
	bool valid_float;
	float value_number;
	std::string date;
	std::string value;
	size_t found;

	valid_date = false;

	found = data_comp.find(" | ");
	date = data_comp.substr(0 , found);
	valid_date = parse_date(date);

	if(found < 0 || valid_date == false)
		std::cout<<RED<<"error inserting : "<<data_comp
			<<": not a good date or not found separator"<<RESET<<std::endl;
	else
	{
		value = data_comp.substr(found + 3);
		valid_float = parse_float(value);
		if(valid_float == false)
			std::cout<<RED<<"error inserting: "<<data_comp
				<<": not a good number"<<RESET<<std::endl;
		else
		{
			try
			{
				value_number =  stod(value);
			}
			catch(std::exception &e)
			{
				std::cout<<"error: "<<e.what()<<std::endl;
			}
			this->_data_to_compare.insert(std::pair<std::string, float>(date, value_number));
		}
	}
}

/*
 * Method to read the data to compare in BitcoinExchange class
 * */
void BitcoinExchange::read_data_to_compare(char * input_file)
{
	std::string data;
	std::ifstream input;

	input.open(input_file);
	if(input.fail() == true)
		std::cout<<"error: cannot open file: "
			<<input_file<<std::endl;

	std::getline(input, data, '\n');
	std::getline(input, data, '\n');
	check_database_io_state(input);
	while(input.eof() == false)
	{
		if(data.empty() == false)
		{
			add_entry_to_compare(data);
		}
		std::getline(input, data, '\n');
		check_database_io_state(input);
	}
	input.close();
}

/*
 * Methods to show values in database
 * */
void BitcoinExchange::show_databases_values(void)
{

	for(std::map<std::string, float>::iterator it = this->_exchange_values.begin();
			it != this->_exchange_values.end(); it++)
	{
		std::cout<<(*it).first<<","<<(*it).second<<std::endl;
	}
}

void BitcoinExchange::show_values_to_compare(void)
{

	for(std::map<std::string, float>::iterator it = this->_data_to_compare.begin();
			it != this->_data_to_compare.end(); it++)
	{
		std::cout<<(*it).first<<" | "<<(*it).second<<std::endl;
	}
}

/*
 * Method to convert values
 * */
void BitcoinExchange::converting_values(void)
{
	std::cout<<BLUE<<"----- Conversions -----"<<RESET<<std::endl;

	for(std::map<std::string, float>::iterator it = this->_data_to_compare.begin();
			it != this->_data_to_compare.end(); it++)
	{
		float *value;
		value = new float;
		*value = -1.0;
		//std::cout<<(*it).first<<" | "<<(*it).second<<std::endl;
		try
		{
			Date d((*it).first);
			Date *aux;
			aux = new Date();

			for(std::map<std::string, float>::iterator it_values = this->_exchange_values.begin(); 
					it_values != this->_exchange_values.end(); it_values++)
			{

				Date provisional((*it_values).first);
				if(provisional < d)
				{
					*aux = provisional;
					break;
				}
			}

			for(std::map<std::string, float>::iterator it_values = this->_exchange_values.begin(); 
					it_values != this->_exchange_values.end(); it_values++)
			{
				Date provisional((*it_values).first);
				if(provisional > (*aux) && provisional <= d)
				{
					*aux = provisional;
					*value = ((*it_values).second);
				}
			}
			delete aux;
		}
		catch(std::exception& e)
		{
			std::cout<<e.what()<<std::endl;
		}

		if((*value) > 0.0f)
		{
			if((*it).second < 1000.0f && (*it).second > 0.0f)
			{
				std::cout<<GREEN<<(*it).first<<" => "<<(*it).second<<" = "<<
					*value<<RESET<<std::endl;
			}
		}
		delete value;
	}
}

/* 
 * Class Date 
 * */
/*
 * Constructor and destructor
 * */

Date::Date(const std::string& date)
{
	parse_date(date);
}

Date::Date(void)
	:_day(0), _month(0), _year(0)
{}

/*
 *Overloading operators of Date class
 * */
bool Date::operator<= (const Date& date) const
{
	if(this->_day == date._day && this->_month == date._month && this->_year == date._year)
		return (true);

	if(this->_year < date._year)
		return true;
	else if(this->_year == date._year && this->_month < date._month)
	{
		return true;
	}
	else if(this->_year == date._year && this->_month == date._month
			&& this->_day < date._day)
	{
		return true;
	}

	return false;
}

bool Date::operator< (const Date& date) const
{
	return (*this <= date);
}

bool Date::operator> (const Date& date) const
{

	if(this->_year > date._year)
		return true;
	else if(this->_year == date._year && this->_month > date._month)
	{
		return true;
	}
	else if(this->_year == date._year && this->_month == date._month
			&& this->_day > date._day)
	{
		return true;
	}

	return false;
}

Date& Date::operator= (const Date& date_cpy)
{
	if(this != &date_cpy)
	{
		this->_day = date_cpy._day;
		this->_month = date_cpy._month;
		this->_year = date_cpy._year;
	}

	return (*this);
}

/*
 * Methods of Date class
 * */

bool Date::parse_date(const std::string& date)
{
	
	int pos;
	int init;
	int field_of_date;
	std::string field;
	int year;
	int month;
	int day;
	int separator;
	std::map<int, int> month_days;

	month_days[1] = 31;
	month_days[3] = 31;
	month_days[4] = 30;
	month_days[5] = 31;
	month_days[6] = 30;
	month_days[7] = 31;
	month_days[8] = 31;
	month_days[9] = 30;
	month_days[10] = 31;
	month_days[11] = 30;
	month_days[12] = 31;	

	init = 0;
	pos = 0;
	field_of_date = 0;
	separator = 0;
	while(date[pos] != '\0')
	{
		if(!(date[pos] >= '0' && date[pos] <= '9') && date[pos] != '-')
			return false;
		if(date[pos] == '-')
		{
			separator++;
			if(field_of_date == 0)
			{
				field = date.substr(init, pos - init);
				if(field.size() > 10 && field.size() < 1)
					return false;
				year = atoi(field.c_str());
			}
			else if(field_of_date == 1)
			{
				field = date.substr(init, pos - init);
				if(field.size() != 2 )
					return false;
					month = atoi(field.c_str());
					if(month <= 0 || month > 12)
						return false;
			}
			else if(separator > 2)
				return false;

			field_of_date++;
			init = pos + 1;
		}
		pos++;
	}
	field = date.substr(init, pos - init);
	
	if(field.size() != 2)
		return false;
	day = atoi(field.c_str());
	if(month != 2)
	{
		if(day > month_days[month] || day < 0)
			return false;
	}
	else
	{
		if((year % 4) == 0)
		{
			if(day > 29 || day < 0)
				return false;
		}
		else if(day > 28 || day < 0)
			return false;

	}
	this->_day = day;
	this->_month = month;
	this->_year = year;
	return true;
}


/*Class BadDateException implementation
 * */

Date::BadDateException::BadDateException(void)
	: _msg("error: Bad Date Exception")
{}

Date::BadDateException::~BadDateException() throw()
{}

const char * Date::BadDateException::what() const throw()
{
	return (this->_msg.c_str());
}
