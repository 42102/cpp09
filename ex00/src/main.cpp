
#include <iostream>
#include "../inc/BitcoinExchange.hpp"

int main(int argc, char *argv[])
{
	
	if(argc == 2)
	{
		BitcoinExchange be;
		be.read_data_to_compare(argv[1]);
		be.converting_values();
	}
	else
	{
		std::cout<<"error: Incorrect number of params"<<std::endl;
	}
	return (0);
}
