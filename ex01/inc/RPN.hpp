
#if !defined(RPN_HPP)
#define RPN_HPP

#include <iostream>
#include <stack>
#include <cstdlib>

int reverse_polish_notation(const std::string& str);
#endif
