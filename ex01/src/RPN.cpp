
#include "../inc/RPN.hpp"

int reverse_polish_notation(const std::string& str)
{
	ssize_t pos; 
	std::stack<float> numbers;

	pos = 0;
	while(str[pos] != '\0')
	{
		if((str[pos]<= '9' && str[pos] >= '0') || str[pos] == ' '
				|| (str[pos] == '+' || str[pos] == '-' || str[pos] == '*' 
					|| str[pos] == '/' ))
		{
			if(str[pos]<= '9' && str[pos] >= '0')
				numbers.push(static_cast<float>(str[pos] - '0'));
			else
			{
				if(str[pos] == '+' || str[pos] == '-' || str[pos] == '*' 
					|| str[pos] == '/')
				{
				float n1, n2;
				if(numbers.empty() == false)
				{
					n2 = numbers.top();
					numbers.pop();	
				}
				else
					return -2;

				if(numbers.empty() == false)
				{
					n1 = numbers.top();	
					numbers.pop();
				}
				else
					return -2;
				switch(str[pos])
				{
					case '+':	
						numbers.push(n1 + n2);
						break;

					case '-':
						numbers.push(n1 - n2);
						break;

					case '*':
						numbers.push(n1 * n2);
						break;

					case '/':
						numbers.push(n1 / n2);
						break;
					}
				}
			}
			pos++;
		}
		else
			return -1;
	}
	
	if(numbers.empty() == false)
		std::cout<<"result: "<<numbers.top()<<std::endl;
	else
		return -3;

	return 0;
}
