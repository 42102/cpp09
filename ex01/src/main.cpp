
#include <iostream>
#include "../inc/RPN.hpp"

int main(int argc, char **argv)
{
	if(argc == 2)
	{
		switch(reverse_polish_notation(argv[1]))
		{
			case -1:
				std::cout<<"error: bad argument"<<std::endl;
				break;

			case -2:
				std::cout<<"error: bad number of number or operands"<<std::endl;
				break;

			case -3:
				std::cout<<"error: bad operation with stack"<<std::endl;
		}
	}
	else
	{
		std::cout<<"error: Bad Number of Parameters"<<std::endl;
	}

	return 0;
}
