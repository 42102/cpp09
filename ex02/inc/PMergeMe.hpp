
#ifndef PMERGE_ME_HPP
#define PMERGE_ME_HPP

#include <iostream>
#include <cstdlib>
#include <vector>
#include <list>
#include <time.h>
#include <iomanip>

//Function to parse numbers
bool parse_numbers(char **argv, int init, int end);

//Functions for vector containers
void merge_sort_vector(std::vector<int>& vec, int p, int r);
void insert_numbers_into_vector(std::vector<int>& vec, char **argv, int init, int end);
void merge_vector(std::vector<int>& vec, int p, int q, int r);

//Functions for vector containers
void merge_sort_list(std::list<int>& list_nums, int p, int r);
void insert_numbers_into_list(std::list<int>& list_nums, char **argv, int init, int end);
void merge_list(std::list<int>& list_nums, int p, int q, int r);

//Methods to show vectors and lists

void show_vector(std::vector<int>& vec, int before);
void show_list(std::list<int>& list_nums, int before);

#endif
