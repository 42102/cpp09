
#include "../inc/PMergeMe.hpp"

/*
 * This method parse if data passed to the program is correct
 * */
bool parse_numbers(char **argv, int init, int end)
{
	char *endptr;
	for(int i = init; i <= end; i++)
	{
		long int number;
		number = strtol(argv[i], &endptr, 10);
		if(endptr == argv[i] || *endptr != '\0')
			return false;

		if(number > INT_MAX || number < INT_MIN)
			return false;

	}
	return true;
}

/*
 * This method insert numbers converted to int into vector passed
 * */
void insert_numbers_into_vector(std::vector<int>& vec, char **argv, int init, int end)
{
	
	for(int i = init; i <= end; i++)
	{
		long int number;
		number = strtol(argv[i], NULL, 10);
		vec.push_back(static_cast<int>(number));
	}
}

/*
 * This method insert numbers converted to int into list passed
 * */
void insert_numbers_into_list(std::list<int>& list_nums, char **argv, int init, int end)
{
	
	for(int i = init; i <= end; i++)
	{
		long int number;
		number = strtol(argv[i], NULL, 10);
		list_nums.push_back(static_cast<int>(number));
	}
}


/*
 * This method sort values in vector taking halves of initial vector
 * till reach an atomic value, then starts to sort this atomic parts
 * */
void merge_sort_vector(std::vector<int>& vec, int p, int r)
{
	int q;
	if(p >= r)
		return ;
	
	q = (p + r)/2;
	merge_sort_vector(vec, p, q);
	merge_sort_vector(vec, q + 1, r);
	merge_vector(vec, p, q, r);
}

/*
 * This method is responsible to sort correctly the elements passed
 * in vector; it is the main part of merge sort algorithm
 * */
void merge_vector(std::vector<int>& vec, int p, int q, int r)
{

	int n1;
	int n2;
	int i;
	int k, j;

	n1 = q - p + 1;
	n2 = r - q;
	
	std::vector<int> aux1(n1);
	std::vector<int> aux2(n2);
	
	i = 0;
	for(int pos = p; pos <= q; pos++)
	{
		aux1[i] = vec[pos];
		i++;
	}


	i = 0;
	for(int pos = q + 1; pos <= r; pos++)
	{
		aux2[i] = vec[pos];
		i++;
	}
	
	k = p;
	i = 0;//related to n1 => aux1
	j = 0;// related to n2 => aux2
	while(i < n1 && j < n2)
	{
		if(aux1[i] <= aux2[j])
		{
			vec[k] = aux1[i];
			i++;
		}
		else
		{
			vec[k] = aux2[j];
			j++;
		}
		k++;
	}

	while(i < n1)
	{
		vec[k] = aux1[i];
		k++;
		i++;
	}


	while(j < n2)
	{
		vec[k] = aux2[j];
		k++;
		j++;
	}
}


/*
 * This method sort values in vector taking halves of initial list
 * till reach an atomic value, then starts to sort this atomic parts
 * */
void merge_sort_list(std::list<int>& list_nums, int p, int r)
{
	int q;
	if(p >= r)
		return ;
	
	q = (p + r)/2;
	merge_sort_list(list_nums, p, q);
	merge_sort_list(list_nums, q + 1, r);
	merge_list(list_nums, p, q, r);
}

/*
 * This method is responsible to sort correctly the elements passed
 * in list; it is the main part of merge sort algorithm
 * */
void merge_list(std::list<int>& list_nums, int p, int q, int r)
{

	std::list<int>::iterator iter, iter1, iter2;

	
	std::list<int> aux1;
	std::list<int> aux2;

	iter1 = list_nums.begin();
	for(int i = 0; i < p; i++)
		iter1++;

	for(int pos = p; pos <= q; pos++)
	{
		aux1.push_back(*iter1);
		iter1++;
	}


	iter2 = list_nums.begin();
	for(int i = 0; i < q + 1; i++)
		iter2++;
	for(int pos = q + 1; pos <= r; pos++)
	{
		aux2.push_back(*iter2);
		iter2++;
	}
	
	iter = list_nums.begin();
	for(int i = 0; i < p; i++)
		iter++;

	iter1 = aux1.begin();//related to n1 => aux1
	iter2 = aux2.begin();// related to n2 => aux2
	while(iter1 != aux1.end() && iter2 != aux2.end())
	{
		if((*iter1) <= (*iter2))
		{
			(*iter) = (*iter1);
			iter1++;
		}
		else
		{
			(*iter) = (*iter2);
			iter2++;
		}

		iter++;
	}
	while(iter1 != aux1.end())
	{
		(*iter) = (*iter1);
		iter++;
		iter1++;
	}


	while(iter2 != aux2.end())
	{
		(*iter) = (*iter2);
		iter++;
		iter2++;
	}
}

//Methods to show vectors and lists

/*
 * Method to show vector, if before < 0 then print before,
 * if before >= 0 then print after
 * */
void show_vector(std::vector<int>& vec, int before)
{
	if(before < 0)
		std::cout<<"Before: ";
	else
		std::cout<<"After: ";

	if(vec.empty() == false)
	{
		for(std::vector<int>::iterator it = vec.begin(); it != vec.end(); it++)
		{
			std::cout<<(*it)<<" ";
		}
	}
	std::cout<<std::endl;
}

/*
 * Method to show list, if before < 0 then print before,
 * if before >= 0 then print after
 * */
void show_list(std::list<int>& list_nums, int before)
{
	if(before < 0)
		std::cout<<"Before: ";
	else
		std::cout<<"After: ";

	if(list_nums.empty() == false)
	{
		for(std::list<int>::iterator it = list_nums.begin(); it != list_nums.end(); it++)
		{
			std::cout<<(*it)<<" ";
		}
	}
	std::cout<<std::endl;
}
