
#include <iostream>
#include "../inc/PMergeMe.hpp"
#define _POSIX_C_SOURCE 199309L
int main(int argc, char **argv)
{
	if(argc >= 3)
	{
		if(parse_numbers(argv, 1, argc - 1) == false)
		{
			std::cout<<"error: bad parameters"<<std::endl;
		}
		else
		{
			std::vector<int> numbers_vector;
			std::list<int> numbers_list;
			struct timespec init;
			struct timespec end;
			float elapsed_time_vector = 0.0f;
			float elapsed_time_list = 0.0f;
			double seconds = 0;

			//Vector operations
			insert_numbers_into_vector(numbers_vector, argv, 1, argc - 1);
			show_vector(numbers_vector, -1);
			
			clock_gettime(CLOCK_MONOTONIC, &init);
			
			merge_sort_vector(numbers_vector, 0, numbers_vector.size() - 1);

			clock_gettime(CLOCK_MONOTONIC, &end);
			elapsed_time_vector = ((float)end.tv_nsec - (float)init.tv_nsec);
			seconds = end.tv_sec - init.tv_sec;
			seconds *= 1.0e6;
			elapsed_time_vector *= 1e-3;
			elapsed_time_vector += seconds;
			
			show_vector(numbers_vector, 1);

			std::cout<<"---------------------------------------------"<<std::endl;
			//List operations
			insert_numbers_into_list(numbers_list, argv, 1, argc - 1);
			show_list(numbers_list, -1);
			
			clock_gettime(CLOCK_MONOTONIC, &init);

			merge_sort_list(numbers_list, 0, numbers_list.size() - 1);


			clock_gettime(CLOCK_MONOTONIC, &end);
			elapsed_time_list = ((float)end.tv_nsec - (float)init.tv_nsec);
			seconds = end.tv_sec - init.tv_sec;
			seconds *= 1.0e6;
			elapsed_time_list *= 1e-3;
			elapsed_time_list += seconds;

			show_list(numbers_list, 1);

			std::cout<<std::setprecision(6)<<std::fixed
				<<"Time to proccess a range of "<<argc - 1<<" elements with "
				<<"std::vector : " <<elapsed_time_vector<<" us"<<std::endl;
			
			std::cout<<std::setprecision(6)<<std::fixed
				<<"Time to proccess a range of "<<argc - 1<<" elements with "
				<<"std::list : " <<elapsed_time_list<<" us"<<std::endl;
		}
	}
	else
	{
		std::cout<<"error: not enought numbers to sort"<<std::endl;
	}
	return 0;
}
